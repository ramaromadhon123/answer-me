<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Ask;
use App\Models\ListWeb;

class AnswerController extends Controller
{
    public function home()
    {
        $data1 = new Ask;
        $data['A'] = $data1->where('flag',0)->count();
        $data2 = new Ask;
        $data2->setConnection('mysql2');
        $data['B'] = $data2->where('flag',0)->count();
        // $model =
        //new ListWeb;
        // $model->setConnection('mysql3');
        // $data = $model->orderBy('flag','ASC')->get();
        return view('home',['data'=>$data]);
    }

    public function tryApi()
    {
        return view('try-api');
    }

    public function ask1()
    {
        $ask = new Ask;
        $asks = $ask->orderBy('flag', 'ASC')->latest()->paginate(5);
    
        return view('ask1',compact('asks'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    public function ask2()
    {
        $ask = new Ask;
        $ask->setConnection('mysql2');
        $asks = $ask->orderBy('flag', 'ASC')->latest()->paginate(5);
    
        return view('ask2',compact('asks'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    public function answer1($id)
    {
        $data1 = new Ask;
        $data = $data1->where('id',$id)->first();
        return view('answer1',compact('data'));
    }

    public function update1(Request $request, $id)
    {
        $ask = Ask::find($id);
        $ask->jawaban = $request->jawaban;
        $ask->flag = 1;
        $ask->update();
        return redirect('ask1')->with('status', 'Pertanyaan berhasil dijawab');
    }

    public function answer2($id)
    {
        $data1 = new Ask;
        $data1->setConnection('mysql2');
        $data = $data1->where('id',$id)->first();
        return view('answer2',compact('data'));
    }

    public function update2(Request $request, $id)
    {
        $ask1 = new Ask;
        $ask1->setConnection('mysql2');
        $ask = $ask1->find($id);
        $ask->jawaban = $request->jawaban;
        $ask->flag = 1;
        $ask->update();
        return redirect('ask2')->with('status', 'Pertanyaan berhasil dijawab');
    }
}
