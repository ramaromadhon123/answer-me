<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AnswerController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('ask1', [AnswerController::class, 'ask1']);
Route::get('ask2', [AnswerController::class, 'ask2']);
Route::get('home', [AnswerController::class, 'home']);
Route::get('answer1/{id}', [AnswerController::class, 'answer1']);
Route::post('answer1/update/{id}', [AnswerController::class, 'update1']);
Route::get('answer2/{id}', [AnswerController::class, 'answer2']);
Route::post('answer2/update/{id}', [AnswerController::class, 'update2']);
Route::get('try-api', [AnswerController::class, 'tryApi']);
