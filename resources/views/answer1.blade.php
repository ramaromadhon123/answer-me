@extends('layout.app')
<head>
    <title>Answer Me!</title>
</head>
@section('content')
<div class="container mt-4">
    <div>
        <a class="btn btn-dark" onclick="history.back(-1)" role="button">Kembali</a>
    </div>
    @if(session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    <div class="card">
        <div class="card-header text-center font-weight-bold">
            Menjawab pertanyaan!
        </div>
        <div class="card-body">
            <form name="add-ask-form" id="add-ask-form" method="post" action="{{url('answer1/update',$data->id)}}">
            @csrf
            <div class="form-group">
                <label for="exampleInputEmail1">Pertanyaan : </label>
                <textarea name="pertanyaan" class="form-control" rows="6" required=""  disabled>{{ $data->pertanyaan }}</textarea>
            </div>

            <div class="form-group">
                <label for="exampleInputEmail1">Jawaban : </label>
                <textarea name="jawaban" class="form-control" rows="6" required="" placeholder="Jawaban...."></textarea>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            </form>
        </div>
    </div>
</div>
@stop