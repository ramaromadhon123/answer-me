@extends('layout.app')
<head>
    <title>Answer Me!</title>
</head>
@section('content')

    <button type="button" id="fetchBtn"
        class="btn btn-primary">
        Fetch Data
    </button>
  
    <div class="container">
        <h1>Employee List</h1>
        <ul id="list"></ul>
    </div>

@endsection

@section('script')
<script>
	let fetchBtn = document.getElementById("fetchBtn");

	fetchBtn.addEventListener("click", buttonclickhandler);

	function buttonclickhandler() {
		// Instantiate an new XHR Object
		const xhr = new XMLHttpRequest();

		// Open an obejct (GET/POST, PATH,
		// ASYN-TRUE/FALSE)
		xhr.open("GET","http://127.0.0.1:8000/api/tickets", true);

		// When response is ready
		xhr.onload = function () {
			if (this.status === 200) {

				// Changing string data into JSON Object
				obj = JSON.parse(this.responseText);

				// Getting the ul element
				let list = document.getElementById("list");
				str = ""

                obj.forEach((item) => {
                    console.log('ID: ' + item.name);
                    str += `<li>${item.name}</li>`;
                });

				list.innerHTML = str;
                
			}
			else {
				console.log("File not found");
			}
		}

		// At last send the request
		xhr.send();
	}
</script>

@endsection

