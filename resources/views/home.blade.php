@extends('layout.app')
<head>
    <title>Answer Me!</title>
</head>
@section('content')

<table id="listWeb" class="table table-bordered table-sm">
    @php
        $i = 0
    @endphp
    <thead class="table-light" align="center">
        <th class="text-center">No</th>
        <th class="text-center" >Web</th>
        <th class="text-center">Belum Terjawab</th>
        <th class="text-center">Aksi</th>
    </thead>
    <tr>
        <td style="width: 4%" align="center">{{ ++$i }}</td>
        <td style="width: 44%">Web A</td>
        <td style="width: 44%">{{ $data['A'] }}</td>
        <td style="width: 8%" align="center"><a class="btn btn-primary" href="{{url('ask1')}}" role="button">Lihat</a></td>
    </tr>
    <tr>
        <td style="width: 4%" align="center">{{ ++$i }}</td>
        <td style="width: 44%">Web B</td>
        <td style="width: 44%">{{ $data['B'] }}</td>
        <td style="width: 8%" align="center"><a class="btn btn-primary" href="{{url('ask2')}}" role="button">Lihat</a></td>
    </tr>
</table>

@stop