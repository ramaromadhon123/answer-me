@extends('layout.app')
<head>
    <title>Answer Me!</title>
</head>
@section('content')
@if(session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif

<table class="table table-bordered">
    <thead class="table-light" align="center">
        <th class="text-center">No</th>
        <th class="text-center">Pertanyaan</th>
        <th class="text-center">Jawaban</th>
        <th class="text-center">Aksi</th>
    </thead>
    <div>
        <a class="btn btn-dark" href="{{url('home')}}" role="button">Kembali</a>
    </div>
    @foreach ($asks as $ask)
    <tr>
        <td style="width: 4%" align="center">{{ ++$i }}</td>
        <td style="width: 44%">{{ $ask->pertanyaan }}</td>
        <td style="width: 44%">{{ $ask->jawaban }}</td>
        @if ($ask->flag == 0)
            <td style="width: 8%" align="center"><a class="btn btn-primary" href="{{url('answer1',$ask->id)}}" role="button">Jawab</a></td>
        @else
            <td style="width: 8%" align="center"><span class="glyphicon glyphicon-ok" style="color: green;"></span></td>
        @endif
    </tr>
    @endforeach
</table>

<div class="d-flex justify-content-end">
    {!! $asks->links() !!}
</div>
@stop